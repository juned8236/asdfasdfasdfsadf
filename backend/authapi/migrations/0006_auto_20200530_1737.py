# Generated by Django 2.2 on 2020-05-30 17:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authapi', '0005_auto_20200530_1734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='last_name',
            field=models.CharField(max_length=100),
        ),
    ]
