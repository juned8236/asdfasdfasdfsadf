import axios from 'axios';
import * as actionTypes from './actionTypes';

export const authStart = () => {
    console.log('start')

    debugger
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = token => {
    console.log('sucess')

    debugger
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token
    }
}

export const authFail = error => {
    console.log('fail')

    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const logout = () => {
    console.log('logout')

    debugger
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
}

export const checkAuthTimeout = expirationTime => {
    console.log('authchecktime')

    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000)
    }
}


export const authLogin = (email, password) => {
    return dispatch => {
        debugger
        dispatch(authStart());
        axios.post('http://localhost:1337/rest-auth/login/', {
            email: email,
            password: password
        })
            .then(res => {
                const token = res.data.key;
                const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
                localStorage.setItem('token', token);
                localStorage.setItem('expirationDate', expirationDate);
                dispatch(authSuccess(token));

                dispatch(checkAuthTimeout(3600));

            })
            .catch(err => {
                dispatch(authFail(err))
            })
    }
}
export const registration = email_link => {
    debugger;
    return {
        type: actionTypes.registration,
        email_link: email_link
    }
}

export const reg_error = regis => {
    return {
        type: actionTypes.reg_error,
        regis: regis
    }
}



export const authSignup = (first_name, last_name, email, password1, password2, is_miner,is_admin, read_only) => {
    return dispatch => {
        dispatch(authStart());
        debugger;

        axios.post('http://localhost:1337/rest-auth/registration/', {
            first_name: first_name,
            last_name: last_name,
            email: email,
            password1: password1,
            password2: password2,
            is_miner: is_miner,
            read_only: read_only,
            is_admin:is_admin
        })
            .then(res => {
                const email_link = res.data;
                dispatch(registration(email_link))
                debugger;


            })
            .catch(err => {
                dispatch(reg_error(err))
            })
    }
}

export const authCheckState = () => {
    console.log('authcheck')
    return dispatch => {
        const token = localStorage.getItem('token');
        if (token === undefined) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                dispatch(authSuccess(token));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
}