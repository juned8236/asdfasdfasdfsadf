import React from "react";
import { Route } from "react-router-dom";
import { connect } from 'react-redux';
import Login from '../component/Login/index';
import Registration from "../component/Registration";
import Dashboard from '../component/Dashboard/index';

const BaseRouter = (...props) => (
  <div>

    <Route exact path="/signup/" component={Registration} />
    <Route exact path="/" component={Login} />      
    <Route exact path="/dashboard" component={Dashboard} />
    
  </div>
);



const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.isAuthenticated
  }
}

export default connect(mapStateToProps)(BaseRouter);