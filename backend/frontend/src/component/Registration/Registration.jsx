import "../Input/Input.css";
import React from "react";
import logo from '../../images/logo.png';
import { NavLink } from 'react-router-dom'
import Input from "../Input/index";
import CircularProgress from '@material-ui/core/CircularProgress';

function template() {
  const { first_name, last_name, email, password1, password2 } = this.state.dataObj;
  let errorEmail = null;
  let first_name_error = null;
  let last_name_error = null;

  if (this.props.regis) {
    first_name_error = (
      <span>{this.props.regis.response.data.first_name}</span>
    );

    last_name_error = (
      <span>{this.props.regis.response.data.last_name}</span>
    );

    errorEmail = (
      <span>{this.props.regis.response.data.email}</span>
    );
  }
  return (
    <div className="body-container">
      {this.props.loading ?
        <CircularProgress color="secondary" /> :
        <div className="admin-entry-section">
          <div className="inner-admin">
            <div className="container-fluid">
              <div className="row align-items-center">
                <div className="col-12 col-md-6 p-0 order-md-12 animated slideInLeft">
                  <div className="inner-section register-section">
                    <img src={logo} className="logo" alt="" />
                    <div className="message-conntainer">
                      <h2 className="text-center">Get started</h2>
                      <h4 className="text-center">It's free to signup and only takes a minute.</h4>
                    </div>
                    <p>You have an account? <NavLink to="/">Sign In</NavLink></p>
                  </div>
                </div>
                <div className="col-12 col-md-6 p-0 animated slideInRight">
                  <div className="form-section">
                    <h2>Sign up</h2>

                    {(this.props && this.props.email_link && this.props.email_link.detail) &&
                      <div className="form-group required-alert-box" style={{ height: 50  }}>
                        <span>Thanks for Registration</span>
                        <span>{this.props.email_link.detail +": Click on that link to activate Account"}</span>
                      </div>}
                    <form>
                      <div className="row">
                        <div className="col-12 col-sm-6">
                          <Input span={first_name_error} label="First name" type='text' className="form-control" id="first_name" val={first_name} placeholder="First name" fnPrepareData={this.fnPrepareData} />
                        </div>
                        <div className="col-12 col-sm-6">
                          <Input span={last_name_error} label="Last name" type='text' className="form-control" id="last_name" val={last_name} placeholder="Last name" fnPrepareData={this.fnPrepareData} />
                        </div>
                      </div>
                      <Input span={errorEmail} label="Email Address" type='text' className="form-control" id="email" val={email} placeholder="Email Address" fnPrepareData={this.fnPrepareData} />
                      <div className="row">
                        <div className="col-12 col-sm-6">
                          <Input label="Password" type='Password' className="form-control" id="password1" val={password1} placeholder="Password" fnPrepareData={this.fnPrepareData} />
                        </div>
                        <div className="col-12 col-sm-6">
                          <Input label="Confirm Password" type='Password' className="form-control" id="password2" val={password2} placeholder="Confirm Password" fnPrepareData={this.fnPrepareData} />
                        </div>
                      </div>

                      {this.props && this.props.regis && this.props.regis.response.data && this.props.regis.response.data.password1 &&
                        <div className="form-group required-alert-box" style={{ height: 10 }}>
                          <span>{"Password : "+this.props.regis.response.data.password1}</span>
                        </div>}

                      {this.props && this.props.regis && this.props.regis.response.data && this.props.regis.response.data.password2 &&
                        <div className="form-group required-alert-box" style={{ height: 10,}}>
                          <span>{"Confirm Password : "+this.props.regis.response.data.password2}</span>
                        </div>}


                      <div className="form-group required-alert-box">
                        <label>Role</label>
                        <select className="form-control" onChange={this.handleChange}>
                          <option value="">Select role</option>
                          <option value="True">Miner</option>
                          <option value="True">Read Only</option>
                        </select>
                      </div>
                      <div className="form-group mb-3">
                        <button type="submit" className="btn btn-green btn-block" onClick={this.submit.bind(this)}>Sign Up</button>
                      </div>

                      {this.props && this.props.regis && this.props.regis.response.data && this.props.regis.response.data.non_field_errors &&
                        <div className="form-group required-alert-box" style={{ height: 10 }}>
                          <span>{this.props.regis.response.data.non_field_errors}</span>
                        </div>} 
                                       </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      }

    </div>



  );
};

export default template;