import React from "react";
import template from "./Registration.jsx";
import * as actions from '../../store/actions/auth';
import { connect } from 'react-redux';

class Registration extends React.Component {

  constructor() {
    super();
    this.state = {
      'dataObj': {
        first_name: '',
        last_name: '',
        email: '',
        password1: '',
        password2: '',
        is_miner: "False",
        read_only: "False",
        is_admin:"False"
      },
    };
    this.toggle = this.toggle.bind(this);
    this.fnPrepareData = this.fnPrepareData.bind(this);
    this.submit = this.submit.bind(this);
    this.handleChange=this.handleChange.bind(this);
  }

  toggle() {
    this.props.app(false)
  }

  fnPrepareData(key, value) {
    this.setState({
      dataObj: {
        ...this.state.dataObj,
        [key]: value
      }
    })
  }
  handleChange = (event) => {
   
    if (event.target.value === "is_miner") {
      this.setState({
        dataObj: {
          ...this.state.dataObj,
          "is_miner": event.target.value
        }
      })
    }
    if (event.target.value === "read_only") {
      this.setState({
        dataObj: {
          ...this.state.dataObj,
          "read_only": event.target.value
        }
      })
    }
  };
  submit() {
    this.props.onAuth(
      this.state.dataObj.first_name,
      this.state.dataObj.last_name,
      this.state.dataObj.email,
      this.state.dataObj.password1,
      this.state.dataObj.password2,
      this.state.dataObj.is_miner,
      this.state.dataObj.read_only,
      this.state.dataObj.is_admin

       )

  }
 
 
  render() {
    return template.call(this);
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    error: state.error,
    regis:state.regis,
    email_link:state.email_link
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (first_name
      ,last_name,email,
      password1,
      password2,
      is_admin,
      is_miner,
      read_only) => dispatch(actions.authSignup(first_name,
        last_name,email,password1,password2,is_miner,read_only,is_admin))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration);