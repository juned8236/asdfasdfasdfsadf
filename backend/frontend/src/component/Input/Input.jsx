import "./Input.css";
import React from "react";

function template() {
  const { label, type, id, val, className, placeholder,password1,password2,span } = this.props
  return (
      <div className="form-group required-alert-box">
        <label>{label}</label>
        <input id={id} type={type} value={val} className={className} password1={password1} password2= {password2} 
        onChange={this.fnChange.bind(this)}  placeholder={placeholder} name=''/>

        <span>{span}</span>
         
      </div>
  );
};

export default template;
