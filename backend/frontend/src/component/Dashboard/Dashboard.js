import React from "react";
import template from "./Dashboard.jsx";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/auth';
class Dashboard extends React.Component {
  submit = () => {
    this.props.logout();
    


  }
  render() {
    return template.call(this);
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.token,
    isAuthenticated:state.isAuthenticated
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(actions.logout()),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);