import React from "react";
import template from "./Login.jsx";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/auth';


class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      'dataObj': {
        email: '',
        password: '',

      },
    };
    this.submit = this.submit.bind(this);
    this.fnPrepareData = this.fnPrepareData.bind(this);



  }
  fnPrepareData(key, value) {
    this.setState({
      dataObj: {
        ...this.state.dataObj,
        [key]: value
      }
    })
  }

  submit() {
    debugger;
    this.props.onAuth(this.state.dataObj.email, this.state.dataObj.password);
//     if(this.props.isAuthenticated){
//       console.log("isAuthenticated")
//             return <Redirect to='/dashboard/' />;

//     }
//     if(!this.props.error){
//       console.log("error")

//       return <Redirect to='/dashboard/' />;

// }if(this.props.token){
//   console.log("token")

//   return <Redirect to='/dashboard/' />;

// }
  }
  // func1(rawdata){
  //   debugger;
  //   if(rawdata !==null){
  //     return <Redirect to='/dashboard/' />;

  //   }   
  // }

 
  render() {
    return template.call(this);
  }
}

const mapStateToProps = (state) => {

  return {
    loading: state.loading,
    error: state.error,
    token:state.token,
    isAuthenticated:state.isAuthenticated,
  }
}

const mapDispatchToProps = dispatch => {
  debugger

  return {
    onAuth: (email, password) => dispatch(actions.authLogin(email, password)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);