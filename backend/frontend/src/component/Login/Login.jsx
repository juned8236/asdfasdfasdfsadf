import "./Login.css";
import React from "react";
import Input from '../Input/index';
import CircularProgress from '@material-ui/core/CircularProgress';
import logo from '../../images/logo.png';
import { NavLink } from 'react-router-dom'
import Dashboard from '../Dashboard/index'
import { Route, Redirect } from "react-router-dom";

function template() {
  const { email, password } = this.state.dataObj;

  let errorMessage = null;
  let errorMessage1 = null;
  let errorMessage2 = null;

  if (this.props.error) {
    errorMessage = (
      <div className="form-group required-alert-box" style={{ height: 0 }}>
        <span >{this.props.error.response.data.non_field_errors}</span>
      </div>
    );

    errorMessage1 = (
      <span>{this.props.error.response.data.password}</span>
    );
    errorMessage2 = (
      <span >{this.props.error.response.data.email}</span>
    );

  }
  if (this.props.isAuthenticated) {
    console.log("isAuthenticated")
    return <>
      <Route >
        <Route exact path="/dashboard" component={Dashboard} />
        <Redirect to='/dashboard'></Redirect>
      </Route></>

  }

  return (
    <div className='root-login'>

      {this.props.loading ?
        <CircularProgress color="secondary" /> :
        <div className="admin-entry-section">
          <div className="inner-admin">

            <div className="container-fluid">
              <div className="row align-items-center">
                <div className="col-12 col-md-6 p-0 animated slideInRight">
                  <div className="inner-section">
                    <img src={logo} className="logo" alt="" />
                    <div className="message-conntainer">
                      <h2 className="text-center">Welcome back!</h2>
                      <h4 className="text-center">Please sign in to continue.</h4>
                    </div>
                    <p>Don't have an account? <NavLink to="/signup">Create an account</NavLink></p>
                  </div>
                </div>
                <div className="col-12 col-md-6 p-0 animated slideInLeft">
                  <div className="form-section">
                    <h2>Sign in</h2>
                    <form>

                      <Input span={errorMessage2} label="Email Address" type='text' className="form-control" id="email" val={email} required="required" placeholder="Email Address" fnPrepareData={this.fnPrepareData} />
                      <Input span={errorMessage1} label="Password" type='password' className="form-control" id="password" val={password} required placeholder="Password" fnPrepareData={this.fnPrepareData} />
                      <div className="form-group mb-4">
                        <div className="row mx-0 justify-content-between align-items-center">
                          <span className="checkbox-custom">
                            <input type="checkbox" name="" id="remember" />
                            <label htmlFor="remember">Remember me</label>
                          </span>
                          <a href="/password-reset/">Forgot password?</a>
                        </div>
                      </div>
                      <div className="form-group mb-3">
                        <button type="submit" className="btn btn-green btn-block" onClick={this.submit.bind(this)}>Sign In</button>
                      </div>


                    </form>
                    {errorMessage}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    </div>

  );
};

export default template;
